


<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>

## About project
This a test project. XML file as database has been implemented. 
The template of XML file and the XSD schema are located on  `app/Components/XmlDBConnection/Init` folder.
The models which could work with database could be found on `app/Models` all models should inherit XmlDatabaseModel::class.

## Local deployment
-  Cloning the git repository 

    ``git clone https://gitlab.com/Vitaliy_Lucky/xml-database.git``

    ``cd xml-database``
   

- Building docker image and install composer/npm dependencies 

    ``docker compose run --rm app composer install && npm install && npm run build``
    
    By default image build for user with ID 1000:1000. If your dev user has another UID and GUD run this command with root container's user

  ``docker compose run --rm --user=root app composer install && npm install && npm run build``


- Prepare Laravel environment variables
  
    ``cp .env.example .env  && docker compose run --rm app php artisan key:generate``


- After installation completed launch app and proxy server by following command: 

    ``docker compose up -d proxy``

The application will be available by URL [http://10.11.0.1](http://10.11.0.1)

The Live version available on [https://xmldatabase.liubimov.org](https://xmldatabase.liubimov.org)

For authorization on Application used 


## Extend the database by new columns

To add the columns to table just open  `database/schema.xsd` - it is responsible for xml parsing and validation
```
<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" elementFormDefault="qualified">
    <!-- Definition for Database -->
    <xs:element name="database">
        <xs:complexType>
            <xs:sequence>
                <xs:element name="employees">
                    <xs:complexType>
                        <xs:sequence>
                            <!-- Define the employee subtag -->
                            <xs:element name="employee" minOccurs="0" maxOccurs="unbounded">
                                <xs:complexType>
                                    <xs:sequence>
                                        <!-- Subtags within the employee tag -->
                                        <xs:element name="name" type="xs:string"/>
                                        <xs:element name="email" type="xs:string"/>
                                        <xs:element name="phone" type="xs:string"/>
                                        
                                         <!-- Add new element HERE  -->
                                    </xs:sequence>
                                    <!-- Attributes for employee -->
                                    <xs:attribute name="age" type="xs:integer"/>
                                    <xs:attribute name="position_id" type="xs:integer"/>
                                    <xs:attribute name="id" type="xs:string"/>
                                    
                                    <!-- Add new element HERE as example on next line -->
                                    <!-- <xs:attribute name="gender" type="xs:string"/>  -->
                                </xs:complexType>
                            </xs:element>
                        </xs:sequence>
                        <xs:attribute name="count" type="xs:integer"/>
                    </xs:complexType>
                </xs:element>
                <xs:element name="positions">
                    <xs:complexType>
                        <xs:sequence>
                            <xs:element name="position" minOccurs="0" maxOccurs="unbounded">
                                <!-- Definition for Position -->
                                <xs:complexType>
                                    <xs:sequence>
                                        <xs:element name="title" type="xs:string"/>
                                    </xs:sequence>
                                    <xs:attribute name="id" type="xs:string" use="required"/>
                                </xs:complexType>
                            </xs:element>
                        </xs:sequence>
                        <xs:attribute name="count" type="xs:integer"/>
                    </xs:complexType>
                </xs:element>
            </xs:sequence>
        </xs:complexType>
    </xs:element>
</xs:schema>
```

## TODO

* UnitTests
* Implement relationships
* Bugfixing
