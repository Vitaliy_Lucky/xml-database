<?php

namespace App\Components\XmlDBConnection;

use App\Models\XmlDatabaseModel;
use Illuminate\Support\Collection;
use SimpleXMLElement;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use function file_exists;
use function get_class;
use function json_decode;
use function json_encode;
use function register_shutdown_function;
use function simplexml_load_file;

/**
 * Class XmlSimpleConnector implements the XMLConnector using SimpleXML extension
 * This implementation doesn't handle the concurrent R/W operations and memory usage optimisation
 *
 * @author  Vitalii Liubimov <vitalii@liubimov.org>
 *
 * @property SimpleXMLElement         $tableRoot
 * @property \SimpleXMLElement|string $elementTag
 *
 * @package App\Components\XmlDBConnection
 */
class SimpleXMLConnector implements XMLConnector
{
    public XmlDatabaseModel $model;

    // xml database handler for all models
    private static SimpleXMLElement|null $xmlHandler;

    // xsd schemas for all models
    private static SimpleXMLElement|null $xsdHandler;

    private static $isLoaded = false;

    // flags represent the status of loaded xml and xsd
    private static bool $xmlSaved = true;

    private static bool $xsdSaved = true;

    private string $xmlFileName = '';

    private string $xsdFileName = '';


    /**
     * @param  array  $config
     */
    public function __construct(public array $config)
    {
        $this->xmlFileName = $config['xml_file_location'];
        $this->xsdFileName = $config['xsd_file_location'];
        // Register auto-save after execution completes
        register_shutdown_function([$this, 'storeToFile']);
    }


    /**
     * @return void
     */
    public function storeToFile()
    {
        if (!self::$xmlSaved) {
            // todo: handle the file change on concurrent process - conflict resolving
            $this->getXmlHandler()->asXML($this->xmlFileName);
            self::$xmlSaved = true;
        }
    }


    /**
     * @param  \App\Models\XmlDatabaseModel  $model
     *
     * @return $this
     */
    public function setModel(XmlDatabaseModel $model)
    {
        $this->model = $model;

        return $this;
    }


    /**
     * @return SimpleXMLConnector
     * @throws \Exception
     */
    public function connect(): static
    {
        $this->loadToMemory();

        $this->model->loadSchema($this->getXmlHandler(), $this->getXsdHandler());

        return $this;
    }


    /**
     * Load the XML and XSD files only once
     *
     * @return $this
     */
    private function loadToMemory()
    {
        //skip the override the document by loading for another models
        if (self::$isLoaded) {
            return $this;
        }

        if (!file_exists($this->xsdFileName)) {
            $xsdInit = "{$this->config['init_db_location']}/schema.xsd";

            copy($xsdInit, $this->xsdFileName);
        }

        if (!file_exists($this->xmlFileName)) {
            $xmlInit = "{$this->config['init_db_location']}/database.xml";

            copy($xmlInit, $this->xmlFileName);
        }

        self::$xmlHandler = simplexml_load_file($this->xmlFileName);
        self::$xsdHandler = simplexml_load_file($this->xsdFileName);
        $xsd = self::$xsdHandler;

        // XSD namespaces
        $namespaces = $xsd->getDocNamespaces();
        $xsd->registerXPathNamespace('xs', $namespaces['xs']);
        self::$isLoaded = true;

        return $this;
    }


    /**
     * @return SimpleXMLElement|null
     */
    public function getXmlHandler(): ?SimpleXMLElement
    {
        return self::$xmlHandler;
    }


    /**
     * @return \SimpleXMLElement|null
     */
    public function getXsdHandler(): ?SimpleXMLElement
    {
        return self::$xsdHandler;
    }


    /**
     *
     * @param  int  $take
     *
     * @return void
     */
    public function get($page = 1, $take = 10, $xpath = ''): Collection
    {
        return collect($this->tableRoot->xpath("$this->elementTag$xpath"))
            ->sortByDesc(fn($element) => (string) $element['id'])
            ->forPage($page, $take)
            ->map(fn($node) => $this->model->init($this->nodeToArray($node)));
    }


    /**
     * @param  string  $id
     *
     * @return XmlDatabaseModel
     */
    public function find(string $id)
    {
        return $this->first("[@id='$id']");
    }


    /**
     * @param  string  $xpath
     *
     * @return ?XmlDatabaseModel
     * @throws \Exception
     */
    public function first(string $xpath = ''): ?XmlDatabaseModel
    {
        $xmlElement = $this->tableRoot->xpath("{$this->elementTag}$xpath")[0] ?? null;

        if (!$xmlElement) {
            return null;
        }

        if (!$this->model->getElement()) {
            // if model is empty using for filling data
            $model = &$this->model;
        } else {
            // else creating the new model
            $model = $this->model->new();
        }

        $model->setElement($xmlElement);

        $rawArray = json_decode(json_encode($xmlElement), true);
        $model->setRawAttributes($rawArray);

        return $model;
    }


    public function count(): int
    {
        return (int) $this->tableRoot['count'] ?? $this->tableRoot->count();
    }


    /**
     * @param  string  $id
     *
     * @return XmlDatabaseModel
     */
    public function findOrFail(string $id): XmlDatabaseModel
    {
        $element = $this->find($id);

        if (!$element) {
            throw new NotFoundHttpException('The model '
                .get_class($this->model)
                .' with id:'.$id
                .' not found');
        }

        return $element;
    }


    /**
     * @return SimpleXMLConnector
     */
    public function save()
    {
        if (!$this->element) {
            $this->model->setElement($this->tableRoot->addChild($this->elementTag));
            $this->model->id = (string) $this->model->generateId();
            // incrementing the counter
            $this->tableRoot['count'] += 1;
        }

        collect($this->model->getAttribute('@attributes') ?: [])
            ->each(fn($value, $key) => $this->element[$key] = $value);
        collect($this->model->getAttributes())->except(['@attributes'])
            ->each(fn($value, $key) => $this->element->{$key} = $value);

        // new model
        if (!$this->model->id) {
            $this->tableRoot['count'] = ++$this->tableRoot['count'];
        }

        self::$xmlSaved = false;

        return $this;
    }


    /**
     * @param  string  $tag
     * @param  string  $id
     *
     * @return void
     */
    public function delete(string $id = '', $xpath = ''): bool
    {
        $id = $id ?: $this->model->id;

        if ($id) {
            $xpath = "[@id='$id']$xpath";
        }

        $elements = $this->tableRoot->xpath("{$this->elementTag}$xpath");

        foreach ($elements as $index => &$node) {
            unset($node[0]);
            $this->tableRoot['count'] -= 1;
            self::$xmlSaved = false;
        }

        return $deleted ?? false;
    }


    /**
     * @return bool
     */
    public function validate(): bool
    {
        return $this->getXmlHandler()->valid();
    }


    /**
     * @param  \SimpleXMLElement  $node
     *
     * @return mixed
     */
    public function nodeToArray(SimpleXMLElement $node)
    {
        return json_decode(json_encode($node), true);
    }


    public function __get($key)
    {
        return match ($key) {
            'tableRoot' => $this->model->getTableRoot(),
            'elementTag' => $this->model->getElementTag(),
            'element' => $this->model->getElement(),
            default => throw new \Exception("Property $key not found on ".self::class),
        };
    }
}
