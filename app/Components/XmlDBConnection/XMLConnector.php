<?php

namespace App\Components\XmlDBConnection;

interface XMLConnector
{
    public function connect();

    public function get($page, $take);

    public function find(string $id);

    public function save();

    public function delete(string $id = '');

    public function validate();

}
