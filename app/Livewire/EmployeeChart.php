<?php

namespace App\Livewire;

use App\Models\Employee;
use Illuminate\Support\Collection;
use Livewire\Component;
use Livewire\Attributes\On;

class EmployeeChart extends Component
{
    public array $labels;
    public array $values;
    public Collection $selected;

    #[On('employee-select')]
    public function itemSelected($isSelected, $id)
    {
        if (!isset($this->selected)) {
            $this->selected = collect();
        }

        if ($isSelected) {
            $this->selected->push(Employee::findOrFail($id)->toArray());
            $this->updateData($this->selected);
        } else {
            $this->selected = $this->selected->where('id', '!=', $id)->values();

            $this->selected->isEmpty()
                ? $this->updateData(Employee::get())
                : $this->updateData($this->selected);
        }

        $this->dispatch('age-chart-updated', labels: $this->labels, values: $this->values);
    }

    public function mount()
    {
        $this->updateData(Employee::get());
    }

    public function render()
    {
        return view('livewire.employee-chart');
    }


    private function updateData(Collection $models) {
        $this->labels = $models->pluck('name')->toArray();
        $this->values = $models->pluck('age')->toArray();
    }
}
