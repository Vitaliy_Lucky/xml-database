<?php

namespace App\Livewire;

use App\Models\Employee;
use Illuminate\Support\Collection;
use Livewire\Component;
use Livewire\Attributes\On;

class EmployeeCreate extends Component
{
    public $useFaker = false;
    public Employee $model;
    public Employee $placeholders;


    public function mount()
    {
        $this->placeholders = Employee::factory()->make();
        $this->model = $this->useFaker ? $this->placeholders : Employee::new();
    }

    #[On('employee-edit')]
    public function edit($id)
    {
        $this->model = Employee::findOrFail($id);
    }

    public function fillFake() {
        $this->model = $this->placeholders;
    }

    public function save()
    {
        if ($this->model->id ?? false) {
            $this->model->save();
            $this->dispatch('employee-created', ['id' => $this->model->id]);
        } else {
            $this->model->save();
        }

        return $this->redirect('/dashboard');
    }

    public function render()
    {
        return view('livewire.employee-form');
    }
}
