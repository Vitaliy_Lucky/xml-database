<?php

namespace App\Livewire;

use App\Models\Employee;
use Illuminate\Support\Collection;
use Livewire\Component;
use Livewire\Attributes\On;
use Livewire\WithPagination;

class EmployeeTable extends Component
{
    use WithPagination;

    public int $total;
    public int $perPage = 10;
    public int $pages;
    public int $currentPage = 1;
    public Collection $employees;
    public Collection $columns;
    public Employee $model;


    public function edit(string $id) {

        $employee = Employee::findOrFail($id);

        $this->dispatch('edit', ['id' => $employee->id]);
    }


    public function mount()
    {
        $this->model = Employee::new();
        $this->columns = $this->model->getColumns();
        $this->total = Employee::count();
        $this->currentPage = 1;
        $this->pages = (int) ceil($this->total / $this->perPage);
        $this->employees = Employee::get($this->currentPage, $this->perPage)->values();
    }

    public function nextPage()
    {
        $this->currentPage++;
        $this->employees = Employee::get($this->currentPage, $this->perPage)->values();
    }

    public function prevPage()
    {
        $this->currentPage--;
        $this->employees = Employee::get($this->currentPage, $this->perPage)->values();
    }


    public function delete(string $id) {
        $employee = Employee::findOrFail($id);
        $employee->delete();
        $this->mount();
    }

    public function render()
    {
        return view('livewire.employee-table');
    }
}
