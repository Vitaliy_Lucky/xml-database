<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Employee
 *
 * @author  Vitalii Liubimov <vitalii@liubimov.org>
 * @package App\Models
 *
 * @property string $profile_photo_url
 * @property string $profilePhotoUrl
 */
class Employee extends XmlDatabaseModel implements AuthenticatableContract
{
    use HasFactory, Authenticatable;

    /**
     * This method dynamically generate the Avatar URL for user
     *
     * @return string
     */
    public function getProfilePhotoUrlAttribute(): string
    {
        return "https://i.pravatar.cc/150?u={$this->email}";
    }


}
