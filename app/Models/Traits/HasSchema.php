<?php

namespace App\Models\Traits;

use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\JsonEncodingException;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

trait HasSchema
{
    public static $isLoaded = false;

    private static Collection $schemaAttributes;

    private static Collection $schemaElements;

    private static \SimpleXMLElement $tableRoot;

    private static \SimpleXMLElement $schemaRoot;

    private static \SimpleXMLElement $schemaElement;

    private static string $elementTag;

    /**
     * - The element on document related to model
     *
     * @var ?\SimpleXMLElement
     */
    private ?\SimpleXMLElement $element = null;


    /**
     * @param $key
     *
     * @return bool
     */
    public function isAttribute($key): bool
    {
        return self::$schemaAttributes->has($key);
    }


    /**
     * @param $key
     *
     * @return bool
     */
    public function isElement($key): bool
    {
        return self::$schemaElements->has($key);
    }


    /**
     *
     * @return $this
     * @throws \Exception
     */
    public function loadSchema(\SimpleXMLElement $xml, \SimpleXMLElement $xsd): static
    {
        if (self::$isLoaded) {
            return $this;
        }

        $table = $this->getTable();
        $elementName = Str::singular($table);

        self::$tableRoot = $xml->xpath("/database/$table")[0] ?? null;

        self::$schemaRoot = $xsd->xpath("//xs:element[@name='{$table}']")[0] ?? null;

        if (!self::$schemaRoot) {
            throw new Exception("The schema for {$table} not found on schema");
        }

        self::$schemaElement = self::$schemaRoot->xpath("//xs:element[@name='{$elementName}']")[0] ?? null;

        if (!self::$schemaElement) {
            throw new Exception("The schema for {$elementName} not found on {$this->xsdFileName}");
        }

        self::$elementTag = self::$schemaElement['name'];

        // retrieving the collection of expected attributes
        self::$schemaAttributes = collect(self::$schemaElement->xpath("xs:complexType/xs:attribute"))
            ->keyBy(fn($el) => $el['name']);

        // retrieving the collection of expected tags
        self::$schemaElements = collect(self::$schemaElement->xpath("xs:complexType/xs:sequence/xs:element"))
            ->keyBy(fn($el) => $el['name']);

        self::$isLoaded = true;

        return $this;
    }


    /**
     * Resolving the attribute type
     *
     * @param $key
     *
     * @return string
     */
    public function getType($key): string
    {
        if ($key === '@attributes') {
            return 'array';
        }

        if ($this->isAttribute($key)) {
            $type = self::$schemaAttributes[$key]['type'] ?? 'string';
        } else {
            $type = self::$schemaElements[$key]['type'] ?? 'string';
        }

        return Str::replaceStart('xs:', '', $type);
    }


    /**
     * @param          $value
     * @param  string  $type
     * @param          $nullable
     *
     * @return float|int|string|null
     */
    public function convertTo($value, string $type, $nullable = true): array|float|int|string|null
    {
        if (\is_null($value) && $nullable) {
            return $value;
        }

        return match ($type) {
            'array' => Arr::wrap($value),
            'string' => "$value",
            'integer' => (int) $value,
            'double' => (float) $value,
            'float' => (float) $value,
            'date' => \is_null($value) ? $value : new Carbon($value),
            'dateTime' => \is_null($value) ? $value : new Carbon($value),
            'time' => \is_null($value) ? $value : new Carbon($value),
            default => $value
        };
    }


    /**
     * @return string
     */
    public function getElementTag(): string
    {
        return self::$elementTag;
    }


    /**
     * @return \SimpleXMLElement
     */
    public function getTableRoot(): \SimpleXMLElement
    {
        return self::$tableRoot;
    }


    public function getElement(): ?\SimpleXMLElement
    {
        return $this->element;
    }


    public function setElement(\SimpleXMLElement $element): void
    {
        $this->element = $element;
    }


    public function getElementAttributes(): Collection
    {
        return static::$schemaAttributes->map(fn($attribute) => (string) $attribute['name'])->values();
    }


    public function getNestedElements(): Collection
    {
        return static::$schemaElements->map(fn($attribute) => (string) $attribute['name'])->values();
    }


    /**
     * Convert the model instance to an array.
     *
     * @return array
     */
    public function toArray()
    {
        $array = [];
        $this->getElementAttributes()->each(function ($attr) use (&$array) {
            if ($this->{$attr}) {
                $array[$attr] = $this->{$attr};
            }
        });
        $this->getNestedElements()->each(function ($elem) use (&$array) {
            if ($this->{$elem}) {
                $array[$elem] = $this->{$elem};
            }
        });

        return $array;
    }


    /**
     * Convert the model instance to JSON.
     *
     * @param  int  $options
     *
     * @return string
     *
     * @throws \Illuminate\Database\Eloquent\JsonEncodingException
     */
    public function toJson($options = 0)
    {
        $json = json_encode($this->jsonSerialize(), $options);

        if (json_last_error() !== JSON_ERROR_NONE) {
            throw JsonEncodingException::forModel($this, json_last_error_msg());
        }

        return $json;
    }


    public function getVisible()
    {
        return static::$schemaAttributes->map(fn($attribute) => (string) $attribute['name'])
            ->merge(static::$schemaElements->map(fn($element) => (string) $element['name']))
            ->values()
            ->toArray();
    }


    public function getHidden()
    {
        return $this->hidden ?? [];
    }

    public function getColumns() {
        $hidden = $this->getHidden();

        return $this->getNestedElements()
            ->merge($this->getElementAttributes())
            ->flip()
            ->except($hidden)
            ->flip();
    }
}
