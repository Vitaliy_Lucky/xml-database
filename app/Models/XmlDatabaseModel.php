<?php

namespace App\Models;

use App\Models\Interfaces\SchemaControl;
use ArrayAccess;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Jsonable;
use Illuminate\Database\Eloquent\JsonEncodingException;
use Illuminate\Database\Eloquent\MissingAttributeException;
use JsonSerializable;
use Livewire\Wireable;
use App\Components\XmlDBConnection\{SimpleXMLConnector, XMLConnector};
use App\Models\Traits\HasSchema;
use Illuminate\Database\Eloquent\Concerns\HasAttributes;
use Illuminate\Support\{Arr, Collection, Str};

abstract class XmlDatabaseModel implements SchemaControl, Jsonable, JsonSerializable, Arrayable, ArrayAccess, Wireable
{
    use HasSchema, HasAttributes {
        HasAttributes::setAttribute as setAttributeBase;
        HasAttributes::getAttribute as getAttributeBase;
    }

    public $hidden = [
      'id',
      'position_id',
    ];

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;
    public $exists = false;
    public $wasRecentlyCreated = false;

    public string $table;

    protected XMLConnector $connection;


    public function __construct(array $data = [])
    {
        // Init the Connector
        $this->connection = new SimpleXMLConnector(config('xml-database'));
        $this->connection->setModel($this)->connect();

        // fill the model data in case it is provided
        if ($data) {
            $this->fill($data);
        }
    }


    /**
     * Get the primary key for the model.
     *
     * @return string
     */
    public function getKeyName()
    {
        return $this->primaryKey;
    }


    /**
     * @return string
     */
    public function getTable(): string
    {
        return $this->table ?? Str::snake(Str::pluralStudly(class_basename($this)));
    }


    /**
     * @return XMLConnector
     */
    public function getConnection(): XMLConnector
    {
        return $this->connection;
    }


    /**
     * Determine if the model uses timestamps.
     *
     * @return bool
     */
    public function usesTimestamps()
    {
        // todo create auto-populated created_at and updated_at fields
        return false;
    }


    /**
     * @param $key
     *
     * @return false
     */
    public function isRelation($key)
    {
        // todo implement relationships
        return false;
    }

    /**
     * @param $key
     *
     * @return false
     */
    public function relationLoaded($key)
    {
        // todo implement relationships
        return false;
    }


    /**
     * Get the value indicating whether the IDs are incrementing.
     *
     * @return bool
     */
    public function getIncrementing()
    {
        return $this->incrementing;
    }

    /**
     * @param $data
     *
     * @return \App\Models\XmlDatabaseModel
     * @throws \Exception
     */
    public static function new($data = [])
    {
        return (new static)->fill($data);

    }

    /**
     * @param $data
     *
     * @return static
     */
    public function init($data) : static
    {
        return (new static)->setRawAttributes($data);
    }


    /**
     * @param  array  $values
     *
     * @return $this
     * @throws \Exception
     */
    public function fill(array $values)
    {
        collect($values)->each(fn($value, $key) => $this->setAttribute($key, $value));

        return $this;
    }


    /**
     * @return $this
     */
    public function save()
    {
        $this->connection->save();

        return $this;
    }


    /**
     * @return \Symfony\Component\Uid\Ulid
     */
    public function generateId()
    {
        return Str::ulid();
    }

    /**
     * @param $key
     * @param $value
     *
     * @return $this|mixed
     * @throws \Exception
     */
    public function setAttribute($key, $value)
    {
        if ($this->isAttribute($key)) {
            $attributes = $this->attributes['@attributes'] ?? [];
            $this->attributes['@attributes'] = array_merge($attributes, [$key => $value]);

            return $this;
        } elseif ($this->isElement($key)) {
            return $this->setAttributeBase($key, $value);
        } else {
            throw new \Exception("The key $key schema is not found");
        }
    }

    public function create(array $data) {
        $newModel = $this->model->new($data);
        $newModel->save();

        return $newModel;
    }

    /**
     * @param $key
     *
     * @return float|int|string|null
     */
    public function getAttribute($key)
    {
        if ($this->isAttribute($key)) {
            $value = $this->attributes['@attributes'][$key] ?? null;
        } else {
            $value = $this->getAttributeBase($key);
        }

        return $this->convertTo($value, $this->getType($key));
    }

    /**
     * Create a new Eloquent Collection instance.
     *
     * @param  array  $models
     * @return Collection
     */
    public function newCollection(array $models = [])
    {
        return new Collection($models);
    }



    /**
     * Convert the object into something JSON serializable.
     *
     * @return mixed
     */
    public function jsonSerialize(): mixed
    {
        return $this->toArray();
    }



    /**
     * Determine if the given attribute exists.
     *
     * @param  mixed  $offset
     * @return bool
     */
    public function offsetExists($offset): bool
    {
        try {
            return ! is_null($this->getAttribute($offset));
        } catch (MissingAttributeException) {
            return false;
        }
    }

    /**
     * Get the value for a given offset.
     *
     * @param  mixed  $offset
     * @return mixed
     */
    public function offsetGet($offset): mixed
    {
        return $this->getAttribute($offset);
    }

    /**
     * Set the value for a given offset.
     *
     * @param  mixed  $offset
     * @param  mixed  $value
     * @return void
     */
    public function offsetSet($offset, $value): void
    {
        $this->setAttribute($offset, $value);
    }

    /**
     * Unset the value for a given offset.
     *
     * @param  mixed  $offset
     * @return void
     */
    public function offsetUnset($offset): void
    {
        unset($this->attributes[$offset]);
    }


    /**
     * Dynamically retrieve attributes on the model.
     *
     * @param  string  $key
     *
     * @return mixed
     */
    public function __get($key)
    {
        return $this->getAttribute($key);
    }


    /**
     * Dynamically set attributes on the model.
     *
     * @param  string  $key
     * @param  mixed   $value
     *
     * @return void
     */
    public function __set($key, $value)
    {
        $this->setAttribute($key, $value);
    }


    /**
     * @param $method
     * @param $arguments
     *
     * @return mixed
     */
    public function __call($method, $arguments)
    {
        if (\method_exists($this->connection, $method)) {
            return $this->connection->$method(...$arguments);
        }

        throw new \BadMethodCallException('Call to undefined method: '.self::class.'::'.$method.'()');
    }


    /**
     * @param $method
     * @param $arguments
     *
     * @return mixed
     */
    public static function __callStatic($method, $arguments)
    {
        return (new static)->$method(...$arguments);
    }


    public function toLivewire()
    {
        return $this->toArray();
    }

    public static function fromLivewire($value)
    {
        if ($value['id'] ?? false) {
            $model = self::find($value['id']);
            $model->fill($value);

            return $model;
        }

        return new static($value);
    }
}
