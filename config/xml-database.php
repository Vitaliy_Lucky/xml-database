<?php

use App\Components\XmlDBConnection\SimpleXMLConnector;

return [
    // database path location
    'xml_file_location' => env('DB_XML_PATH', database_path('database.xml')),

    'xsd_file_location' => env('DB_XML_SCHEMA_PATH', database_path('schema.xsd')),

    'init_db_location' => base_path('app/Components/XmlDBConnection/Init'),

    'connector' => SimpleXMLConnector::class,

    /** Time in seconds for sleep for waiting the resource release on concurrent R/W operations */
    'concurrent_delay' => 0.5,
    'concurrent_max_wait' => 3, //Time for sleep for waiting the concurrent process to load

];
