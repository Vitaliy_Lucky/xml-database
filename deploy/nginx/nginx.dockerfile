FROM nginx:1.25.2-alpine

COPY ./deploy/nginx/nginx.conf /etc/nginx/conf.d/default.conf
RUN  mkdir -p /var/www/public && touch /var/www/public/index.php
