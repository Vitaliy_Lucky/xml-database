ARG IMAGE_VERSION="0.1.7"
ARG IMAGE

FROM ${IMAGE}:${IMAGE_VERSION} as test

ARG APP_VERSION
ENV APP_VERSION "${APP_VERSION}"

RUN echo Building ${APP_VERSION}
COPY ./deploy/php-fpm/tests.bootstrap.sh /usr/bin/test
RUN chmod +x /usr/bin/test
RUN apt-get install -y nodejs wget && apt install -y npm


# Image for local development xdebug enabled
FROM ${IMAGE}-dev:${IMAGE_VERSION} as xdebug

RUN apt-get update && apt-get install -y nodejs wget && apt install -y npm
#RUN install-php-extensions xdebug
#ENV PHP_INI_SCAN_DIR="${PHP_INI_SCAN_DIR}:/var/www/deploy/config/xdebug"

# image from test
FROM test as build
RUN echo "Building app with php-fpm image: ${IMAGE}:${IMAGE_VERSION}"

COPY . /var/www

RUN composer install --no-progress --optimize-autoloader --no-dev -q
RUN php artisan view:cache && php artisan config:clear
RUN apt-get clean
USER www
