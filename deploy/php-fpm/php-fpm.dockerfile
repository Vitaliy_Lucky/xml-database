FROM php:8.2-fpm as api

# Copy composer.lock and composer.json
#COPY composer.lock composer.json /var/www/

# Set working directory
WORKDIR /var/www

# Install dependencies
RUN apt-get update && apt-get install -y build-essential libpng-dev libjpeg62-turbo-dev libfreetype6-dev \
    locales zip jpegoptim optipng pngquant gifsicle vim unzip git curl libonig-dev libzip-dev libgd-dev \
    libpq-dev netcat-openbsd procps

# Clear cache
RUN apt-get clean && rm -rf /var/lib/apt/lists/*
#Mine
COPY --from=mlocati/php-extension-installer /usr/bin/install-php-extensions /usr/local/bin/

RUN pecl install redis && docker-php-ext-enable redis

# Install extensions
RUN docker-php-ext-install pdo pdo_pgsql pdo_mysql mbstring zip exif pcntl
RUN docker-php-ext-configure gd --with-external-gd
RUN docker-php-ext-install gd
# Install redis and composer
RUN install-php-extensions @composer yaml

# Add user for laravel application
RUN groupadd -g 1000 www
RUN useradd -u 1000 -ms /bin/bash -g www www

RUN apt-get update && apt-get  install -y nodejs wget && apt install -y npm

# Expose port 9000 and start php-fpm server
EXPOSE 9000
CMD ["php-fpm"]


# Image for local development xdebug enabled
FROM api as xdebug

RUN install-php-extensions xdebug

# install helpers
RUN apt-get update && apt-get install -y netcat-openbsd procps iputils-ping dnsutils nano
#ENV PHP_INI_SCAN_DIR="${PHP_INI_SCAN_DIR}:/var/www/deploy/config/xdebug"

# image from test
FROM api as test

COPY ./deploy/php-fpm/tests.bootstrap.sh /usr/bin/test
RUN chmod +x /usr/bin/test


# Build image stage
FROM test as build

COPY . /var/www

RUN composer install --no-progress --optimize-autoloader --no-dev -q
RUN php artisan view:cache && php artisan config:clear
