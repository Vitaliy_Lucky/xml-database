#!/usr/bin/bash

set -e
echo "Composer install"
composer install -q --no-progress || exit 1

echo "Run migrations"
php artisan migrate:fresh --seed -q || exit 2
echo "Migration completed"

echo "Test execution start"
php artisan test || exit 3
