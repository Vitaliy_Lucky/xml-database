<div class="col-span-full xl:col-span-12 bg-white dark:bg-slate-800 shadow-lg rounded-sm border border-slate-200
  dark:border-slate-700">
    <header class="px-5 py-4 border-b border-slate-100 dark:border-slate-700">
        <h2 class="font-semibold text-slate-800 dark:text-slate-100">Employees</h2>
    </header>
    <div class="p-3">

        <!-- Table -->
        <div class="overflow-x-auto">
            <table class="table-auto w-full">
                <!-- Table header -->
                <thead class="text-xs font-semibold uppercase text-slate-400 dark:text-slate-500 bg-slate-50 dark:bg-slate-700 dark:bg-opacity-50">
                <tr>
                    <th class="p-2 whitespace-nowrap">
                        <div class="font-semibold text-left">Name</div>
                    </th>
                    <th class="p-2 whitespace-nowrap">
                        <div class="font-semibold text-left">Email</div>
                    </th>
                    <th class="p-2 whitespace-nowrap">
                        <div class="font-semibold text-left">Phone</div>
                    </th>
                    <th class="p-2 whitespace-nowrap">
                        <div class="font-semibold text-center">Age</div>
                    </th>
                    <th class="p-2"></th>
                </tr>
                </thead>
                <!-- Table body -->
                <tbody class="text-sm divide-y divide-slate-100 dark:divide-slate-700">
                @foreach($employees as $employee)
                    <tr>
                        <td class="p-2 whitespace-nowrap">
                            <div class="flex items-center">
                                <div class="w-10 h-10 shrink-0 mr-2 sm:mr-3">
                                    <img class="rounded-full" src="{{$employee->profile_photo_url}}" width="40"
                                         height="40"
                                         alt="{{$employee->name}}"/>
                                </div>
                                <div class="font-medium text-slate-800 dark:text-white">{{$employee->name}}</div>
                            </div>
                        </td>
                        <td class="p-2 whitespace-nowrap">
                            <div class="text-left">{{$employee->email}}</div>
                        </td>
                        <td class="p-2 whitespace-nowrap">
                            <div class="text-left font-medium text-green-500">{{$employee->phone}}</div>
                        </td>
                        <td class="p-2 whitespace-nowrap">
                            <div class="text-base text-center">{{$employee->age}}</div>
                        </td>
                        <td class="p-2">
                            <div class="text-base flex flex-row flex-nowrap items-center justify-center">
                                <button class="fa-solid fa-edit"
                                        wire:click="$dispatch('employee-edit', {id: '{{$employee->id}}' })"></button>
                                <button class="fa-solid fa-trash pl-2"
                                        wire:click="delete('{{$employee->id}}')"
                                ></button>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>

    </div>
</div>
