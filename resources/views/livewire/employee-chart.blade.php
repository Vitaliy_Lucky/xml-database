<div class="flex flex-col col-span-full xl:col-span-6 bg-white dark:bg-slate-800 shadow-lg rounded-sm border border-slate-200 dark:border-slate-700">
    <header class="px-5 py-4 border-b border-slate-100 dark:border-slate-700">
        <h2 class="font-semibold text-slate-800 dark:text-slate-100">Teammate's ages (click on teammate's name on table to filter the data)</h2>
    </header>
    <div id="dashboard-card-04-legend" class="px-5 py-3">
        <ul class="flex flex-wrap"></ul>
    </div>

    <div class="h-auto p-6 w-full"
         x-data="{
                chart: null,
                labels: $wire.labels,
                ages: $wire.values,
                initChart() {
                     this.chart = new Chart(document.getElementById('age-chart').getContext('2d'), {
                        type: 'bar',
                        data: {
                          labels: this.labels,
                          datasets: [{
                            label: 'Employee ages',
                            data: this.ages,
                            backgroundColor: 'rgb(99, 102, 241)',
                            borderWidth: 1
                          }]
                        },
                        options: {
                            indexAxis: 'y',
                            responsive: false
                        }
                      });
            } }"
         x-init="
            initChart();
            "
    x-on:age-chart-updated.window="
          labels = $event.detail.labels;
          ages = $event.detail.values;
          chart.destroy();
          initChart();
       "
    >
        <canvas id="age-chart" class="w-11/12 h-auto ml-auto mr-auto"></canvas>
    </div>

</div>
