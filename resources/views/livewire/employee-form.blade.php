<div x-show="modalFormOpen"
     class="fixed inset-0 z-50 overflow-y-auto"
     aria-labelledby="modal-title" role="dialog" aria-modal="true"
>
    <div class="flex items-end justify-center min-h-screen px-4 text-center md:items-center sm:block sm:p-0">
        <div x-cloak @click="modalFormOpen = false" x-show="modalFormOpen"
             x-transition:enter="transition ease-out duration-300 transform"
             x-transition:enter-start="opacity-0"
             x-transition:enter-end="opacity-100"
             x-transition:leave="transition ease-in duration-200 transform"
             x-transition:leave-start="opacity-100"
             x-transition:leave-end="opacity-0"
             class="fixed inset-0 transition-opacity bg-gray-500 bg-opacity-40" aria-hidden="true"
        ></div>

        <div x-cloak x-show="modalFormOpen"
             x-transition:enter="transition ease-out duration-300 transform"
             x-transition:enter-start="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
             x-transition:enter-end="opacity-100 translate-y-0 sm:scale-100"
             x-transition:leave="transition ease-in duration-200 transform"
             x-transition:leave-start="opacity-100 translate-y-0 sm:scale-100"
             x-transition:leave-end="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
             class="inline-block w-full max-w-xl p-8 my-20 overflow-hidden text-left transition-all transform
             bg-white dark:bg-slate-800 rounded-lg shadow-xl 2xl:max-w-2xl"
        >
            <div class="flex items-center justify-between space-x-4">
                <h1 class="text-xl font-medium text-gray-800 dark:text-white">
                    {{ ($model['id'] ?? false) ? 'Update team member\'s data'  : 'Invite team member' }}
                </h1>

                <button @click="modalFormOpen = false" class="text-gray-600 focus:outline-none hover:text-gray-700">
                    <svg xmlns="http://www.w3.org/2000/svg" class="w-6 h-6" fill="none" viewBox="0 0 24 24"
                         stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                              d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z"/>
                    </svg>
                </button>
            </div>

            <p class="mt-2 text-sm text-gray-500 ">
                Add your teammate to your team and start work to get things done
            </p>

            <form class="mt-5" wire:submit="save">
                @if ($model['id'] ?? false)
                    <input class="hidden" wire:model="model.id"/>
                @endif
                @foreach($model->getColumns() as $column)
                    @if ($model->getType($column) == 'integer')
                            <div class="mt-4">
                                <label for="phone" class="block text-sm text-gray-700 capitalize dark:text-gray-200">
                                    Teammate {{$column}}</label>
                                <input placeholder="{{$placeholders->$column}}"
                                       type="number"   wire:model="model.{{$column}}" name="{{$column}}"
                                       class="block w-full px-3 py-2 mt-2 text-gray-600 placeholder-gray-400 bg-white
                                       border border-gray-200 rounded-md focus:border-indigo-400 focus:outline-none
                                       focus:ring focus:ring-indigo-300 focus:ring-opacity-40">
                            </div>
                        @else
                        <div class="mt-4">
                            <label for="{{$column}}"
                                   class="block text-sm text-gray-700 capitalize dark:text-gray-200">Teammate {{$column}}</label>
                            <input placeholder="{{$placeholders[$column]}}" type="text"
                                   wire:model="model.{{$column}}" name="{{$column}}"
                                   class="block w-full px-3 py-2 mt-2 text-gray-600 placeholder-gray-400
                                   bg-white border border-gray-200 rounded-md focus:border-indigo-400
                                   focus:outline-none focus:ring focus:ring-indigo-300 focus:ring-opacity-40
                            ">
                        </div>
                        @endif
                @endforeach

                <div class="flex justify-end mt-6">
                    @if (!($model['id'] ?? false))
                        <button type="button"
                                class="px-3 py-2 text-sm tracking-wide text-white capitalize transition-colors duration-200 transform bg-indigo-500 rounded-md dark:bg-indigo-600 dark:hover:bg-indigo-700
                                   dark:focus:bg-indigo-700 hover:bg-indigo-600 focus:outline-none focus:bg-indigo-500 focus:ring focus:ring-indigo-300 focus:ring-opacity-50 mr-6"
                                wire:click="fillFake"
                        >

                            Fill by fake data
                        </button>
                    @endif

                    <button type="submit"
                            class="px-3 py-2 text-sm tracking-wide text-white capitalize transition-colors duration-200 transform bg-indigo-500 rounded-md dark:bg-indigo-600 dark:hover:bg-indigo-700
                                   dark:focus:bg-indigo-700 hover:bg-indigo-600 focus:outline-none focus:bg-indigo-500 focus:ring focus:ring-indigo-300 focus:ring-opacity-50"
                    >

                        {{ ($model['id'] ?? false) ? 'Save Member' : 'Invite Member' }}
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>


