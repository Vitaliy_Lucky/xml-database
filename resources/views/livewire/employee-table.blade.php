<div class="col-span-full xl:col-span-12 bg-white dark:bg-slate-800 shadow-lg rounded-sm border border-slate-200
  dark:border-slate-700">
    <header class="px-5 py-4 border-b border-slate-100 dark:border-slate-700">
        <h2 class="font-semibold text-slate-800 dark:text-slate-100">Employees</h2>
    </header>
    <div class="p-3">

        <!-- Table -->
        <div class="overflow-x-auto">
            <table class="table-auto w-full">
                <!-- Table header -->
                <thead class="text-xs font-semibold uppercase text-slate-400 dark:text-slate-500 bg-slate-50 dark:bg-slate-700 dark:bg-opacity-50">
                <tr>
                    @foreach($columns as $column)
                        <th class="p-2 whitespace-nowrap ">
                            <div class="font-semibold
                                        {{ $model->getType($column) == 'integer' ? 'text-center' : 'text-left'}}"
                            >{{Str::of($column)->ucfirst()}}</div>
                        </th>
                    @endforeach
                    <th class="p-2"></th>
                </tr>
                </thead>
                <!-- Table body -->
                <tbody class="text-sm divide-y divide-slate-100 dark:divide-slate-700">
                @foreach($employees as $employee)
                    <tr x-data="{selected: false}"
                        x-bind:class="selected ? 'bg-indigo-600' : ''">
                        @foreach($columns as $column)
                            @if ($column == 'name')
                                <td class="p-2 whitespace-nowrap cursor-pointer"
                                    x-on:click="selected = !selected; $dispatch('employee-select', { isSelected: selected, id: '{{$employee->id}}'} )">
                                    <div class="flex items-center">
                                        <div class="w-10 h-10 shrink-0 mr-2 sm:mr-3">
                                            <img class="rounded-full" src="{{$employee->profile_photo_url}}" width="40"
                                                 height="40"
                                                 alt="{{$employee->name}}"/>
                                        </div>
                                        <div class="font-medium text-slate-800 dark:text-white">{{$employee->name}}</div>
                                    </div>
                                </td>
                            @elseif($model->getType($column) == 'integer')
                                <td class="p-2 whitespace-nowrap">
                                    <div class="text-base text-center">{{($employee->$column)}}</div>
                                </td>
                            @else
                                <td class="p-2 whitespace-nowrap">
                                    <div class="text-left">{{$employee->$column}}</div>
                                </td>
                            @endif
                        @endforeach
                        <td class="p-2">
                            <div class="text-base flex flex-row flex-nowrap items-center justify-center">
                                <button class="fa-solid fa-edit"
                                        wire:click="$dispatch('employee-edit', {id: '{{$employee->id}}' })"></button>
                                <button class="fa-solid fa-trash pl-2"
                                        wire:click="delete('{{$employee->id}}')"
                                ></button>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="w-full flex flex-row justify-center align-center pt-2">
                <div class="flex-flex-row justify-around">
                    <i class="fa fa-chevron-left cursor-pointer" wire:click="prevPage"
                       x-bind:class="$wire.currentPage <= 1 ? 'hidden' : ''"
                    ></i>
                    <span>{{$currentPage}}</span>
                    <i class="fa fa-chevron-right cursor-pointer"
                       wire:click="nextPage"
                       x-bind:class="($wire.currentPage >= ($wire.pages)) ? 'invisible' : ''"
                    >

                    </i>
                </div>
            </div>
        </div>

    </div>
</div>
